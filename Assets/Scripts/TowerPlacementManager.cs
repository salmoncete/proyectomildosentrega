using UnityEngine;

public class TowerPlacementManager : MonoBehaviour
{
    public GameObject[] towerPrefabs; // Array de prefabs de torres
    public LayerMask placementPointLayer; // Capa de los puntos de colocación

    // Este método puede ser llamado desde la UI para establecer el prefab que se va a arrastrar
    public void SetTowerPrefab(int index)
    {
        if (index >= 0 && index < towerPrefabs.Length)
        {
            // Encuentra el objeto UI que maneja el arrastre y suelta, y establece el prefab
            Drag dragHandler = FindObjectOfType<Drag>();
            if (dragHandler != null)
            {
                dragHandler.torrePrefab = towerPrefabs[index];
            }
        }
    }
}