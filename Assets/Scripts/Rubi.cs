using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rubi : MonoBehaviour
{
    public int valor = 1;
    
    //Solo se puede recoger monedas al hacer click
    private void OnMouseDown()
    {
        GameManager.Instance.SumarRubis(valor);
        Destroy(this.gameObject);
    }

}
