using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TiendaController : MonoBehaviour
{
    //Se declara la clase de scriptableObject que manjea el nivel de las torres, y botones para añadir nive a las torres
    public NivelTorres niveles;
    public GameObject botonTorre1;
    public GameObject botonTorre2;
    public GameObject botonTorre3;
    public GameObject botonTorre4;

    void Start()
    {
        //Cuando se inicar el juevo se activa el boton de la torre1
        botonTorre1.SetActive(true);
    }

    //aqui se defien los metodos para comprar las torres segun su nivel
    public void comprarTorre1()
    {
        if(GameManager.Instance.totalRubis >= 1)
        {
            niveles.T1 = 2;
            botonTorre1.SetActive(false);
            GameManager.Instance.RestarRubis(1);
        }
    }
    public void comprarTorre2()
    {
        if (GameManager.Instance.totalRubis >= 2)
        {
            //Se actualiza el nivel de la torre y se desactiva el boton luego se eliminala cantidad definida para cada torre
            niveles.T2 = 2;
            botonTorre2.SetActive(false);
            GameManager.Instance.RestarRubis(2);
        }
    }

    public void comprarTorre3()
    {
        if (GameManager.Instance.totalRubis >= 3)
        {
            niveles.T3 = 2;
            botonTorre3.SetActive(false);
            GameManager.Instance.RestarRubis(3);
        }
    }

    public void comprarTorre4()
    {
        if (GameManager.Instance.totalRubis >= 4)
        {
            niveles.T4 = 2;
            botonTorre4.SetActive(false);
            GameManager.Instance.RestarRubis(4);
        }
    }
}
