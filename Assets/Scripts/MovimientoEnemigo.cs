using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.SceneManagement;
using Random = UnityEngine.Random;

public class MovimientoEnemigo : MonoBehaviour
{
    //Estos son los atributos de nuestros enemigos, tiene velocidad vida, asi como tmbn el camino deficinod que tienen que recorrer
    [SerializeField] private float moveSpeed;
    private int vida;
    
    [Header("Referencias")] 
    [SerializeField] private Rigidbody2D rb;
    
    private Transform objetivo;
    private Transform[] _Waypoints;
    private int caminoIndex = 0;
    private Transform torre;

    //Se declaran los objetos para poder llmarlos en el metodo ontrigues y poder referenciarlas en el momento de generar moendaso rubis
    [SerializeField] private GameObject monedaprefab;
    [SerializeField] private GameObject diamanteprefab;
    
    //Se declara la probabilidad de que se cree ese recurso
    public int probabilidadmoneda = 50;
    public int probabilidadiamante = 10;

    //Se cargar los datos del enemigo segun su scriptableobject
    public void Load(ClaseEnemigo enemigoInfo, Transform[] waypoints)
    {
        _Waypoints = waypoints;
        caminoIndex = 0;
        objetivo = _Waypoints[0];
        transform.position = objetivo.position;
        moveSpeed = enemigoInfo.moveSpeed;
        vida = enemigoInfo.vida;
        GetComponent<Animator>().runtimeAnimatorController = enemigoInfo.controllerType;
    }

    private void Update()
    {
      
       // aqui creo que podemos poner lo de dani
        if (Vector2.Distance(objetivo.position, transform.position) <= 0.1f)
        {
            caminoIndex++;

            if (caminoIndex >= _Waypoints.Length)
            {
                SpawnerEnemigo.onEnemyDestroy.Invoke(); // llama a la funcion que va restando a los enemigos que vemos
                // en el script SpawnerEnemigo
               Destroy(gameObject);
                return;
            }
            else
            {
                objetivo = _Waypoints[caminoIndex];
            }
        }
    }

    //Dertermina el movimiento del enemigo para que vaya hacia el objetico
    private void FixedUpdate()
    {
            Vector2 direccion = (objetivo.position - transform.position).normalized;
            rb.velocity = direccion * moveSpeed;
        
    }
    
    //Comporbacion de colisiones con un objeto que tenga trigguer, dependiendo de si es una bala se le resta vida y si e smenor a 0 
    //se destruye y genera monedas, y nos suma puntos de recompensa
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("bullet"))
        {
            vida--;

            if (vida <= 0)
            {
                SpawnerEnemigo.EnemyDestroyed();
                Destroy(gameObject);
                GenerarRecurso();
                GameManager.Instance.SumarPuntos(1);
            }
        }
    }
    
    //Metodo para generar monedas o rubis de forma aleatoria

    private void GenerarRecurso()
    {
        //Genera un numero entre a y 100 por que son enteros
        float valorrandom = Random.Range(0, 101);
        
        //Determinar que recurso se crea
        if (valorrandom <= probabilidadmoneda)
        {
            Instantiate(monedaprefab, transform.position, Quaternion.identity);
        }
        //Se suma las dos probabilidades para asegurarnos de que las probabilidades totales no superen el 100
        else if (valorrandom <= probabilidadiamante +probabilidadmoneda)
        {
            Instantiate(diamanteprefab, transform.position, Quaternion.identity);
        }
        
        
    }
}
