using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CambioEscena : MonoBehaviour
{
    public void ChangeScene(string aa)
    {
        SceneManager.LoadScene(aa);
    }

    public void Quit()
    {
        UnityEditor.EditorApplication.isPlaying = false;

        Application.Quit();
    }
}
