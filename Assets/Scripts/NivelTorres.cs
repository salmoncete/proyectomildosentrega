using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu]
public class NivelTorres : ScriptableObject
{
    //Script que se utiliza para definir el nivel de las torres del juego
    public int T1;
    public int T2;
    public int T3;
    public int T4;
}
