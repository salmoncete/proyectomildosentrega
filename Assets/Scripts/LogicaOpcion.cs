using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LogicaOpcion : MonoBehaviour
{

    public ControladorDeOpciones panelOpciones;
    //Hace referencia al controlador de opciones
   // public ControladorOpciones panelOpciones;
    void Start()
    {
        
        //Aqui encuentsa el Gameobject con el tag y asi puede recuperar el script de ControladorOpciones 
        //y cargarlo en la sigueinte escena
       panelOpciones = GameObject.FindGameObjectWithTag("Opciones").GetComponent<ControladorDeOpciones>();
    }

    // Update is called once per frame
    void Update()
    {
       
    }
    //este metodo lo utilizamos para poder recagar las opciones entre escenas, el set active activa las opciones.
    public void MostrarOpciones()
    {
        panelOpciones.pantallaOpciones.SetActive(true);
    }
}
