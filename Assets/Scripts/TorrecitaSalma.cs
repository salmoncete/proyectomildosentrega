using UnityEngine;
using UnityEngine.UI;

public class TorrecitaSalma : MonoBehaviour
{
    [Header("References")]
    public LayerMask capaEnemigo; //señalamos en que capa estaan los enemigos para manejar de forma mas efectiva las colisiones
    public GameObject bulletPrefab;
    public Sprite spriteNivel2;

    [Header("Atributos")]
    public float radioDisparo; //radio de disparo de la torre
    public float disparoPorSegundo; //frecuencia con la que se dispara
    public float tiempoVidaNivel1; // Tiempo de vida para el nivel 1
    public float tiempoVidaNivel2;
    public int precio;
    public int tipoTorre;
    public NivelTorres nt; //referencia a la clase que maneja los niveles de la torre
    private int nivel; //nivel actual de la torre

    //Contorla el objetico y los tiempos que hay entre disparo y disparo
    private Transform target;
    private float timeUntilFire;


    private void Start()
    {
        //Inicializa el nivel de la torre segun el tipo
        if (tipoTorre == 1) nivel = nt.T1;
        if (tipoTorre == 2) nivel = nt.T2;
        if (tipoTorre == 3) nivel = nt.T3;
        if (tipoTorre == 4) nivel = nt.T4;

        
        //Aqui se manejan los niveles y se establecen los tiempo de vida segun el nivel y toma un nuevo script segun eso
        if (nivel == 2)
        {
            tiempoVidaNivel2 = 30;
            GetComponent<SpriteRenderer>().sprite = spriteNivel2;

        }
    
        //Al pasar el timepo definico se llama a destruir la torre
        Invoke("DestroyTower", GetTiempoVida());



    }
  //control de tiempo de vida segun el nivel de la torre
    private float GetTiempoVida()
    {
        switch (nivel)
        {
            case 1:
                return tiempoVidaNivel1;
            case 2:
                return tiempoVidaNivel2;
            // Agregar más casos según sea necesario para otros niveles
            default:
                return tiempoVidaNivel1; // Valor predeterminado
        }
    }

    private void Update()
    {
        // Actualizar la posición del Slider en cada frame
        //Busca un bjetivo si no tiene uno ya

        if (target == null)
        {
            FindTarget();
            return;
        }

        //Si el enemigo esta fuera del rango lo reinicia, sino dispara si es el mmento definido
        if (CheckTargetIsInRange())
        {
            target = null;
        }
        else
        {
            timeUntilFire += Time.deltaTime;
            if (timeUntilFire >= 1f / disparoPorSegundo)
            {
                Shoot();
                timeUntilFire = 0f;
            }
        }
    }

    private void Shoot()
    {
        GameObject bulletObj = Instantiate(bulletPrefab, transform.position, Quaternion.identity);
        Bullet bulletScript = bulletObj.GetComponent<Bullet>();
        bulletScript.SetTarget(target);
    }

    //Encuentra nuevos objetivos si entran en el radio de disparo esto se va ejecutando en cada frame por eso se declara en el uopdate
    private void FindTarget()
    {
        RaycastHit2D[] hits = Physics2D.CircleCastAll(transform.position, radioDisparo, Vector2.zero, 0f, capaEnemigo);
        if (hits.Length > 0)
        {
            target = hits[0].transform;
        }
    }

    private bool CheckTargetIsInRange()
    {
        return target != null && Vector2.Distance(target.position, transform.position) <= radioDisparo;
    }

    //Dibujo de el radio del disparo en el editor
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(transform.position, radioDisparo);
    }

    // Método para destruir la torre después de cierto tiempo
    private void DestroyTower()
    {
        Destroy(gameObject);
    }


}