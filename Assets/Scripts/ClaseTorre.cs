using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu]
public class ClaseTorre : ScriptableObject
{
    
    //Es otro ScriptableObject para crear nuevas torres en funcion de lops atributos definidos
    
     public float radioDisparo;
     public float disparoPorSegundo;
     public int vida;
     public int tamany;
     public Sprite foto;
     public GameObject bulletPrefab; 
    // public GameObject prefab; // Prefab del objeto que se va a instanciar
}
