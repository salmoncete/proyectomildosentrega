using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Moneda : MonoBehaviour
{ 
    public int valor = 2;
    
    //solo se puede sumar rubis al hacer click y se eliminan los objetos al hacerlo
    private void OnMouseDown()
    {
        GameManager.Instance.SumarMonedas(valor);
        Destroy(this.gameObject);
    }
}
