using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Volumen : MonoBehaviour
{
    //Estas variables nos serviran para controlar el menu de volumen
    public Slider slider;

    public float sliderValor;

    public Image imagenMute;
    // Start is called before the first frame update
    void Start()
    {
        //Primero crearemos el valor que nos servira para almacenar la posicion de nuestro slider de volumen
        //el valor de 0.5 es con el que iniciara al empezar
        //El audioListener nos sirve para obetener el volumen de nuestro slider
        //y la funcion controlMute, nos ayudara a identificar si el volumen esta apagado o no 
        //y se creara una imagne solo a modo visual de que no hay audio
        slider.value = PlayerPrefs.GetFloat("volumenAudio", 0.5f);
        AudioListener.volume = slider.value;
        ControlarMute();
    }

    public void ChangeSlider(float valor)
    {
        //una vez se tiene el valor actual de nuestro slider lo asignaremos a nuestra variable volumenAudio
        //esto se hace para poder contorlador si hay audio o no 
        sliderValor = valor;
        PlayerPrefs.SetFloat("volumenAudio", sliderValor);
        AudioListener.volume = valor;
        ControlarMute();
    }

    private void ControlarMute()
    {
        if (imagenMute != null)
        {
            if (sliderValor == 0)
            {
                imagenMute.enabled = true;
            }
            else
            {
                imagenMute.enabled = false;
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
