using UnityEngine;
using UnityEngine.UI;

public class PausarJuego : MonoBehaviour
{
    public GameObject pausePanel;

    private bool isPaused = false;

    void Start()
    {
        isPaused = false; // Asegurar que el juego no comience pausado
        Time.timeScale = 1f; // Asegurar que el tiempo del juego esté corriendo normalmente al inicio
        pausePanel.SetActive(false); // Asegurar que el panel de pausa esté desactivado al inicio
    }

    void Update()
    {
        // Verificar si el juego está pausado y si se hizo clic en la pantalla
        if (isPaused && Input.GetMouseButtonDown(0))
        {
            ResumeGame(); // Reanudar el juego
        }
    }

    public void TogglePause()
    {
        isPaused = !isPaused;

        if (isPaused)
        {
            Time.timeScale = 0f; // Pausa el tiempo del juego
            pausePanel.SetActive(true); // Muestra el panel de pausa
        }
        else
        {
            // No es necesario hacer nada aquí, el juego se reanudará cuando se haga clic en la pantalla
        }
    }

    public void ResumeGame()
    {
        isPaused = false; // Establecer que el juego ya no está pausado
        Time.timeScale = 1f; // Reanudar el tiempo del juego
        pausePanel.SetActive(false); // Ocultar el panel de pausa
    }
}