using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReduccioVida : MonoBehaviour
{
    //Desde aqui lo que controlamos es la forma visual en la que se reduce la barra de vida segun se llama al gameobject y a su funciones
    
    public BarraVida barravida;

   // public int vidaActual;
    
    // Start is called before the first frame update
   /* void Start()
    {
        vidaActual = barravida.vida;
    }*/

   //Si un enemigo coliciona con el objeto que se encuentra en el punto final del camino de cada mapa significara que el enemigo logro ns hace daño y nos resta 10 de vida
    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("enemigo"))
        {
            barravida.ReducirVida(10);
        }
        
    }
}
