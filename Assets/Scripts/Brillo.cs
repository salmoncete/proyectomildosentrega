using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//Recordar utilizar la libreria using UnityEngine.UI;, porque sino no puedes acceder a las variables de UI
public class Brillo : MonoBehaviour
{
    //Aqui utilizaremos en panel que creamos para controlar el brillo, utilizaremos su alpha
    public Slider slider;

    public float sliderValor;

    public Image panelBrillo;
    
    void Start()
    {
        // Como en el volumen utilizaremos la logica de almacenar la ultima configuracion que dejamos en el juego
        //Ademas pondremos por defecto un valor al iniciar el juego
        //Para evitar que la pantalla se ponga totalmente en negro dfiniremos el vaor max a 0.9
        slider.value = PlayerPrefs.GetFloat("brillo", 0.5f);
        panelBrillo.color = new Color(panelBrillo.color.r, panelBrillo.color.g, panelBrillo.color.b, slider.value);

    }

    public void modificarSlider(float valor)
    {
        //Para modificar el alpha utilizaremos la porpieda de new Color primero se asginan los colores primarios
        //y el ultimo valor es el alpha que hemos definico que se modificara con el controlador.value
        //con esto tmbn se podria modificar el color
        sliderValor = valor;
        PlayerPrefs.SetFloat("brillo", sliderValor);
       
        panelBrillo.color = new Color(panelBrillo.color.r, panelBrillo.color.g, panelBrillo.color.b, valor);
    }
}
