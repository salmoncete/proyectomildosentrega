using UnityEngine;
using UnityEngine.UI;

public class SliderController : MonoBehaviour
{
    private Slider slider;
    private float volumenGuardado = 0.5f; // Volumen por defecto

    void Start()
    {
        // Obtener referencia al componente Slider
        slider = GetComponent<Slider>();

        // Inicializar el slider con el valor guardado en PlayerPrefs
        slider.value = PlayerPrefs.GetFloat("volumenAudio", volumenGuardado);

        // Suscribirse al evento OnValueChanged del slider para detectar cambios en su valor
        slider.onValueChanged.AddListener(ChangeVolume);
    }

    // Método para cambiar el volumen
    private void ChangeVolume(float valor)
    {
        // Guardar el valor del volumen en PlayerPrefs
        PlayerPrefs.SetFloat("volumenAudio", valor);

        // Aplicar el volumen al AudioListener
        AudioListener.volume = valor;
    }
}
