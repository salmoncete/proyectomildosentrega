using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;

public class VolumenController : MonoBehaviour
{
    private Slider volumenSlider; // Referencia al slider de volumen
    private static VolumenController instance;

    private void Awake()
    {
        // Busca el slider de volumen en la escena actual
        volumenSlider = FindObjectOfType<Slider>();


        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
    }

    private void Update()
    {
        // Si se encontró el slider de volumen, ajusta el volumen del audio
        if (volumenSlider != null)
        {
            float nuevoVolumen = volumenSlider.value;
            AdjustVolume(nuevoVolumen);
        }
    }

    private void AdjustVolume(float nuevoVolumen)
    {
        // Aquí debes ajustar el volumen del audio en tu juego
        // Esto podría implicar llamar a un método específico en tu AudioManager
        // o utilizar las funciones de Unity para ajustar el volumen del audio.

        // Por ejemplo, si tienes un AudioManager con un método SetMusicVolume:
        // AudioManager.Instance.SetMusicVolume(nuevoVolumen);

        // O si estás utilizando directamente las funciones de Unity:
        AudioListener.volume = nuevoVolumen;
    }
}
