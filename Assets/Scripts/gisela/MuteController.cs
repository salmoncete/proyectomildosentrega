using UnityEngine;

public class MuteController : MonoBehaviour
{
    private bool isMuted = false; // Variable para rastrear si el juego está actualmente silenciado

    // Método que se llama cuando se hace clic en el botón
    public void ToggleMute()
    {
        isMuted = !isMuted; // Cambiar el estado de mute

        // Si el juego está silenciado, establecer el volumen a 0, si no poner el volumen guardado en el volumencontroller
        if (isMuted)
        {
            AudioListener.volume = 0f;
        }
        else
        {
            float volumenGuardado = PlayerPrefs.GetFloat("volumenAudio", 0.5f); // Obtener el volumen guardado
            AudioListener.volume = volumenGuardado; // poner el volumen guardado
        }
    }
}
