using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    //utilizamos el header para agrupar los atributos
    [Header("Atributos")]
    [SerializeField] private Rigidbody2D rb;
    public float bulletSpeed;
 
    private Transform target;
    
    //el metodo lo utilizamos para determinar el objetivo del proyectil
    public void SetTarget(Transform _target)
    {
        target = _target;
    }

    private void Update()
    {
        if (IsOutOfScreen())
        {
            Destroy(gameObject);
        }
    }

    //Este metodo se utiliza sobretodo cuando quieres llamarlo en peridoos fijos 
    private void FixedUpdate()
    {
        //Si no encuentra un objetivo sale del metodo
        if (!target) return;
        
        //Aqui calculamos la direccion hacia el objetivo y hacemos que su magnitud sea 1 y configuramos la velocidad
        //del proyectil en funcion de la direccion del objetivo
        Vector2 direction = (target.position - transform.position).normalized;
        this.transform.up = direction; // hacer que el bullet mire en posici�n del enemigo
        direction.Normalize(); //normalizamos el vector de la direccion
        rb.velocity = direction * bulletSpeed;

    }

    /*
   private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("enemigo"))
        {
            MovimientoEnemigo enemy = other.gameObject.GetComponent<MovimientoEnemigo>();
            if (enemy != null)
            {
                enemy.enemigote.vida--;
                if (enemy.enemigote.vida <= 0)
                {
                    SpawnerEnemigo.EnemyDestroyed();
                    Destroy(other.gameObject);
                }
            }
            Destroy(this.gameObject);
        }
       
    }
    */

    //El metodo se llamara cuando el proyectil colicione un un objeto que tenga collider con la opcion is trigger
    private void OnTriggerEnter2D(Collider2D other)
    {
        //Comprobamos que ha colisionado con el enemigo para luego destruir el enemigo
        if (other.gameObject.CompareTag("enemigo"))
        {
            if(other.gameObject.TryGetComponent<MovimientoEnemigo>(out MovimientoEnemigo enemy))
            {
                Destroy(this.gameObject);
                
            }
        }
        if (other.gameObject.tag == "pared")
        {
            Destroy(this.gameObject);
        }
    }


    private bool IsOutOfScreen()
    {
        Vector3 screenPoint = Camera.main.WorldToViewportPoint(transform.position);
        return screenPoint.x < 0 || screenPoint.x > 1 || screenPoint.y < 0 || screenPoint.y > 1;
    }
}
