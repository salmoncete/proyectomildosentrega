using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class MenuInterfaz : MonoBehaviour
{

    //Utilizamos la referencia al animatos por el menu tiene una animacion de ocultarse y de desplegarse
    [SerializeField] private Animator anim;
    
    //variable para definir si esta abierto o cerrado
    private bool isOpen;
 
    //Se controla que si esta abierto de cierre y utiliza la animacion abierto
    public void ToggleMenu()
    {
        isOpen = !isOpen;
        anim.SetBool("abierto", isOpen);
    }
}
