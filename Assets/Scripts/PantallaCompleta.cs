using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PantallaCompleta : MonoBehaviour
{
    //Esta es la casilla para seleccionar pantallacompleta
    public Toggle toggle;
    
    // Start is called before the first frame update
    void Start()
    {
        //Aqui comprobamos si la opcion esta marcada o no, asi que cuando empezemos la partida si
        //detecta que la pantalla esta completa la modifica a modo ventana

        if (Screen.fullScreen)
        {
            toggle.isOn = true;
        }
        else
        {
            toggle.isOn = false;
        }
    }

//Lo que hace esta funcion es activar la pantalla
    public void ActivarPantalla(bool pantallaCompleta) {
        Screen.fullScreen = pantallaCompleta;
    }
    
}
