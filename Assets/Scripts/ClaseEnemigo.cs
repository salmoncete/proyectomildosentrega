using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor.Animations;

[CreateAssetMenu]
public class ClaseEnemigo : ScriptableObject
{
    //Es un scriptableObject que no permite crear nuevos enemigos utilizando una plantilla donde solo tocaremos sus atributos
   //public GameObject enemyPrefab;
    public float moveSpeed;
    public int vida;
    public AnimatorController controllerType;
    public Sprite sprite;

}
