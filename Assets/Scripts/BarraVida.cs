
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class BarraVida : MonoBehaviour
{
    //Asigno la vida total a 100, a modificar segun lo veamos
    public int vida = 100;
    //Forma visual para ver estado de la vida, cambiara segun vida restante
    public Color colorverde = Color.green;
    public Color colornaranja = Color.yellow;
    public Color colorrojo = Color.red;
    //Se le pasa la ui image para poder utilizarla en los metodos de actualizar
    private Image barraVida;

    //Se declara el delegado
    public delegate void VidaReducidaEvent(int cantidad);
    //Evento con parametro para modidifcar la vida
    public event VidaReducidaEvent vidareducida;

    public void Start()
    {
        //Se utiliza el componente imagen de quien lleve este script
        barraVida = GetComponent<Image>();
        //Se suscribe al evento
        vidareducida += ActualizarBarraVida;
        //Se llama a la funcion pasando parametro ocmo lo pide el evento
        ActualizarBarraVida(vida);
    }

    private void ActualizarBarraVida(int vidaActual)
    {
        float porcentajeVida = (float)vidaActual / 100f;
        barraVida.rectTransform.localScale = new Vector3(porcentajeVida, 1, 1);

        if (vidaActual == 0)
        {
            SceneManager.LoadScene("GameOver");
        }
        else if (vidaActual <= 20)
        {
            barraVida.color = colorrojo;
        }
        else if (vidaActual < 50)
        {
            barraVida.color = colornaranja;
        }
        else
        {
            barraVida.color = colorverde;
        }
    }
    public void ReducirVida(int cantidad)
    {
        vida = Mathf.Max(vida - cantidad, 0);
        vidareducida?.Invoke(vida); 
    }
}
