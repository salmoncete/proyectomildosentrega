using System.Collections;
using UnityEngine;
using UnityEngine.Events;

public class SpawnerEnemigo : MonoBehaviour
{
    public delegate void GameEndAction();
    public static event GameEndAction OnGameEnd;
    
    public delegate void RoundChangeAction(int round);
    public static event RoundChangeAction OnWaveChange; // Delegado para notificar cambios en la ronda
    
    [Header("Referencias")] 
    [SerializeField] private ClaseEnemigo[] enemyPrefabs; // Para poder poner diferentes enemigos
    [SerializeField] private GameObject enemigo; //Prefab del enemigo negerico
    
    //Control de tiempos, frecuencia, y cantidad de enemigos que se spawnearan
    
    [Header("Atributos")]
    [SerializeField] private int baseEnemies;
    [SerializeField] private float enemiesPerSecond = 0.5f;
    [SerializeField] private float timeBetweenWaves;
    [SerializeField] private float difficultScailingFactor = 0.75f;
    [SerializeField] private int totalWaves; // Número total de oleadas

    //Evento que se llamara cuando un enemigo se destruya
    
    [Header("Events")]
    public static UnityEvent onEnemyDestroy = new UnityEvent();

    //Control de enemigos vivos y de cuantos quedan por spawnear, contorlador de si se estan spawneando en el momento
    
    private int currentWave = 1;
    private float timeSinceLastSpawn;
    public static int enemiesAlive;
    private int enemiesLeftToSpawn;
    private bool isSpawning = false;

    //Suscribir el metodo enemydestroyed al eventro on enemy destroy
    private void Awake()
    {
        onEnemyDestroy.AddListener(EnemyDestroyed);
    }

    //inicializar la corrutitna para controlar los tiempos de spawneo
    private void Start()
    {
        StartCoroutine(StartWave());
    }

    private void Update()
    {
        if (!isSpawning) return; // No spawnea nada entonces devuelve nada
        timeSinceLastSpawn += Time.deltaTime; // Actualiza el tiempo transcurrido desde el último spawn

        //si apso el tiempo que se requiere y aun quedan enemigos por generar spqanea uno y controla
        //cuantos quedan pendientes y cuantos ya se han spawneado
        if (timeSinceLastSpawn >= (1f / enemiesPerSecond) && enemiesLeftToSpawn > 0)
        {
            EnemySpawn();
            enemiesLeftToSpawn--; // Decrementa el contador de enemigos restantes por spawnear
            enemiesAlive++;
            timeSinceLastSpawn = 0f;
        }

        //Si ya se acabaron de generar los enemigos se termina la oleada
        if (enemiesAlive == 0 && enemiesLeftToSpawn == 0)
        {
            EndWave();
        }
    }

    public static void EnemyDestroyed()
    {
        enemiesAlive--;
    }

    //Se declarala corrutina para iniciar nueva oleada, espera el tiempo que se necesita y habilita el spawn de enemigos
    private IEnumerator StartWave()
    {
        
        yield return new WaitForSeconds(timeBetweenWaves);
        isSpawning = true;
        enemiesLeftToSpawn = EnemiesPerWave();
        OnWaveChange?.Invoke(currentWave); // Notifica el cambio de ronda al iniciar una nueva oleada
    }

    //Se terminala oleada, se reinicia el timepo desde le ultimo spawn y se increments el numero de oleadas
    private void EndWave()
    {
        Debug.Log("Fin de la oleada " + currentWave);

        isSpawning = false;
        timeSinceLastSpawn = 0f;
        currentWave++;

        if (currentWave > totalWaves)
        {
            EndGame(); // Llama a la función que muestra el mensaje de fin de juego
        }
        else
        {
            StartCoroutine(StartWave());
            OnWaveChange?.Invoke(currentWave);
        }
    }
    
    //Muestar mensaje por consola y dispara el evento de fin de partida
    
    private void EndGame()
    {
        Debug.Log("Fin del juego"); 
        OnGameEnd?.Invoke();
    }

    private void EnemySpawn()
    {
        // Selecciona aleatoriamente un prefab de enemigo para spawnear
        
        ClaseEnemigo enemyToSpawn = enemyPrefabs[Random.Range(0, enemyPrefabs.Length)];
        GameObject newEnemigo = Instantiate(enemigo);
        newEnemigo.GetComponent<MovimientoEnemigo>().Load(enemyToSpawn, LevelManager.main.camino);
    }

    private int EnemiesPerWave()
    {
        // Cuanto más alto sea difficultScailingFactor, más rápido se spawnearán enemigos por oleada
        // Oleada 1 = 8 enemigos, oleada 2 = 8*2(oleada)^0.75 = 14 (redondeado)
        return Mathf.RoundToInt(baseEnemies * Mathf.Pow(currentWave, difficultScailingFactor));
    }
}
