using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class botones : MonoBehaviour
{
    //Funciones para carfar diferentes escenas
    public void ChangeScene(string aa)
    {
        SceneManager.LoadScene(aa);
    }

    public void Menu()
    {
        SceneManager.LoadScene("Menu");
    }

    public void Jugar()
    {
        SceneManager.LoadScene("Nivel1");
    }
}

