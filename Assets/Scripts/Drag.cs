using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.EventSystems;

public class Drag : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    public GameObject torrePrefab; // Referencia al prefab de la torre
    private GameObject torreActual; // Referencia a la torre instanciada
    public LayerMask placementPointLayer; // Capa de los puntos de colocación
   
   
    // Este método se llama cuando comienza el arrastre
    public void OnBeginDrag(PointerEventData eventData)
    {
        if ( torrePrefab.GetComponent<TorrecitaSalma>().precio <= GameManager.Instance.totalMonedas)
        {
            // Instanciar el objeto al comenzar el arrastre usando el prefab del ScriptableObject
            torreActual = Instantiate(torrePrefab);

            // Obtener la posición del mouse y pasarlo del Canvas al juego
            Vector2 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);

            // Establecer la posición del objeto instanciado en la posición del mouse
            torreActual.transform.position = mousePosition;
        }
       
    }

    // Este método se llama mientras el objeto se está arrastrando
    public void OnDrag(PointerEventData eventData)
    {
        // Si el objeto está instanciado, moverlo siguiendo el cursor
        if (torreActual != null)
        {
            // Obtener la posición del mouse y pasarlo del Canvas al juego
            Vector2 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);

            // Actualizar la posición del objeto instanciado a la posición del mouse
            torreActual.transform.position = mousePosition;
        }
    }

    // Este método se llama cuando se suelta el objeto que se está arrastrando
    public void OnEndDrag(PointerEventData eventData)
    {
        if (torreActual != null)
        {
            Vector2 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            Collider2D hitCollider = Physics2D.OverlapPoint(mousePosition, placementPointLayer);

            if (hitCollider != null && hitCollider.CompareTag("PlacementPoint"))
            {
                if (hitCollider.transform.childCount == 0)
                {
                    // Establecer la posición final de la torre en el punto de colocación
                    torreActual.transform.position = hitCollider.transform.position;
                    torreActual.transform.parent = hitCollider.transform; // Hacer que la torre sea hija del punto de colocación
                    GameManager.Instance.RestarMonedas(torreActual.GetComponent<TorrecitaSalma>().precio);
                   
                }
                else
                {
                    // Si ya hay una torre en el punto de colocación, destruir la torre instanciada
                    Destroy(torreActual);
                }
            }
            else
            {
                // Si no se suelta en un punto de colocación válido, destruir la torre instanciada
                Destroy(torreActual);
            }

            // Dejar de referenciar el objeto
            torreActual = null;
        }
    }
}
