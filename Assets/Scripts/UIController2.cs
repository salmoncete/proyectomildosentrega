using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class UIController2 : MonoBehaviour
{
   //Desde aqui lo que hacemos es acceder a la UI para actualizar lo que s eve por opantalla y luego llamamos 
   //al singleton de GameMAanager y tulizamos las funciones deficnidas en su script hay que utilizar el ToString 
   //por que lo que se almacena es .text y asi luego lo podemos utilizar
   
    public TextMeshProUGUI puntos;
    public TextMeshProUGUI monedas;
    public TextMeshProUGUI rubis;
    
    // Update is called once per frame
    void Update()
    {
        monedas.text = GameManager.Instance.totalMonedas.ToString();
        rubis.text = GameManager.Instance.totalRubis.ToString();
        puntos.text = "Puntuacion:" + GameManager.Instance.puntosTotales.ToString();
    }

    public void ActualizarMonedas(int totalmonedas)
    {
        monedas.text = totalmonedas.ToString();
    }
    public void ActualizarPuntos(int totalpuntos)
    {
        puntos.text = totalpuntos.ToString();
    }
    public void ActualizarRubis(int totalrubis)
    {
        puntos.text = totalrubis.ToString();
    }
}
