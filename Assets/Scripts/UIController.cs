using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UIController : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI numoleada; // Referencia al objeto que mostrará el número de la ronda
    [SerializeField] private TextMeshProUGUI textowin; // Referencia al objeto  que mostrará el mensaje de victoria

    private void Start()
    {
        // Suscribe el método ShowRound al evento OnRoundChange
        SpawnerEnemigo.OnWaveChange += oleada;

        // Suscribe el método ShowWinMessage al evento OnGameEnd
        SpawnerEnemigo.OnGameEnd += mensajewin;
    }

    // Método que se ejecutará cuando se desencadene el evento OnRoundChange
    private void oleada(int wave)
    {
        numoleada.text = "Ronda: " + wave; // Actualiza el texto del mensaje en pantalla con el número de la ronda
    }

    // Método que se ejecutará cuando se desencadene el evento OnGameEnd
    private void mensajewin()
    {
        if (textowin != null)
        {
            
            // Obtener el nombre de la escena actual
            string currentSceneName = SceneManager.GetActiveScene().name;

            if (currentSceneName != "Nivel3")
            {
                textowin.text = "SIGUIENTE NIVEL"; // Actualiza el texto del mensaje de victoria en pantalla solo si no es nivel3
            }

            // Verificar si la escena actual es nivel1
            if (currentSceneName == "Nivel1")
            {
                // Invoca la carga de la escena nivel2 después de 2 segundos
                Invoke("escenalvl2", 2f);
            }
            // Verificar si la escena actual es nivel2
            else if (currentSceneName == "Nivel2")
            {
                // Invoca la carga de la escena nivel3 después de 2 segundos
                Invoke("escenalvl3", 2f);
            }
            else if (currentSceneName == "Nivel3")
            {
                // Invoca la carga de la escena nivel3 después de 2 segundos
                Invoke("escenaganado", 2f);
            }
        }
    }

    // Método para cargar la escena nivel2
    private void escenalvl2()
    {
        SceneManager.LoadScene("Nivel2");
    }

    // Método para cargar la escena nivel3
    private void escenalvl3()
    {
        SceneManager.LoadScene("Nivel3");
    }

    private void escenaganado()
    {
        SceneManager.LoadScene("Ganado");
    }

    private void OnDestroy()
    {
        SpawnerEnemigo.OnWaveChange -= oleada;
        SpawnerEnemigo.OnGameEnd -= mensajewin;
    }
}
