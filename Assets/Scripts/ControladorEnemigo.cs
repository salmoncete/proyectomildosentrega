using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.SceneManagement;
using Random = UnityEngine.Random;

public class ControladorEnemigo : MonoBehaviour
{
    
    
    [SerializeField] private float moveSpeed;
    [SerializeField] private int vida;
    
    [Header("Referencias")] 
    [SerializeField] private Rigidbody2D rb;
    
    private Transform objetivo; //Referencia a la posicion del enemigo
    private Transform[] _Waypoints; //En un array con los puntos que seguiran los enemigos para desplzarse por el mapa
    private int caminoIndex = 0; // define la posicion del waypoint actual
    private Transform torre;
    public int valormoneda = 10;
    public GameObject prefabmoneda;
    public int cantidadmonedas; // Esta cantidad se genera al morir un enemigo
    

    //Con esta funcion lo que hacemos en cargar la informacion dek enmigo asignarle una possicon determinar la vida etc, 
    //estas variables las igualamos al scriptableObjecte que ClaseEnemigo
    public void Load(ClaseEnemigo enemigoInfo, Transform[] waypoints)
    {
        _Waypoints = waypoints;
        caminoIndex = 0;
        objetivo = _Waypoints[0];
        transform.position = objetivo.position;
        moveSpeed = enemigoInfo.moveSpeed;
        vida = enemigoInfo.vida;
        GetComponent<Animator>().runtimeAnimatorController = enemigoInfo.controllerType;
    }

    //Aqui lo que comprobamos es is el enemigo ha alcanzdo el obejtivo del camino , si lo ha hecho se elimina y reinicia el nuevo objetivo 
    private void Update()
    {
      
        // aqui creo que podemos poner lo de dani
        if (Vector2.Distance(objetivo.position, transform.position) <= 0.1f)
        {
            caminoIndex++;

            if (caminoIndex >= _Waypoints.Length)
            {
                SpawnerEnemigo.onEnemyDestroy.Invoke(); // llama a la funcion que va restando a los enemigos que vemos
                // en el script SpawnerEnemigo
                Destroy(gameObject);
                return;
            }
            else
            {
                objetivo = _Waypoints[caminoIndex];
            }
        }
    }

    //Aqui esta calculando la direccion hacia el objetivo y ajusta la velocidad del rigidbody2D
    private void FixedUpdate()
    {
        Vector2 direccion = (objetivo.position - transform.position).normalized;
        rb.velocity = direccion * moveSpeed;
        
    }
    
    
    //Lo que ocmprobamos son las colisiones si ha sido con una bala se le resta vida  luego comporbamos si es iferiro a 0 y de ser asis
    //eleminamos el opbjeto generamos monedas de recompensa y actualizamos la vida desde nuestro Gamemanager que actua como un singleton
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("bullet"))
        {
            vida--;

            if (vida <= 0)
            {
                SpawnerEnemigo.EnemyDestroyed();
                Destroy(gameObject);
                generarMonedas();
                GameManager.Instance.SumarPuntos(1);
            }
        }
       
    }

    private void generarMonedas()
    {
        //esto lo hacemos por que quremos que se generen un numero de monedas especificas
        for (int i = 0; i < cantidadmonedas; i++)
        {
            //Aqui indicamos de forma aleatoria donde se generara las monedas 
            Vector3 posicionmoneda = transform.position + new Vector3(Random.Range(-1f, 1f), Random.Range(-1f, 1f), 0f);
            //el Quatermion.identity es una constante que no modifica en nada la rotacion.
            Instantiate(prefabmoneda, posicionmoneda, Quaternion.identity);
        }
    
    }
}
