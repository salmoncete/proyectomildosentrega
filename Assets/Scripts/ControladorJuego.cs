using System;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;

    public UIController2 canva;

    // Esto sumara las monedas
    public int totalMonedas;
    // Esto sumara los puntos
    public int puntosTotales;
    // Esto sumara los rubis
    public int totalRubis;

    private void Awake()
    {
        // Aqui comprobamos si la variable no se ha creado aun se le asigna este objeto
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else if (Instance != this)
        {
            Destroy(gameObject);
            return;
        }

        // Suscribirse al evento de cambio de escena
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    private void OnDestroy()
    {
        // Desuscribirse del evento de cambio de escena
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }

    private void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        // Reasignar canva al cargar una nueva escena
        canva = FindObjectOfType<UIController2>();

        if (canva == null)
        {
            totalMonedas = 0;
            puntosTotales = 0;
            totalRubis = 0;
        }



    }


    // Estas son las funciones que se llamaran dependiendo que se este controlando por ejemplo si obtenemos monedas
    // sumaremos puntos y si colocamos una torre en la escena del mapa se nos descontara las monedas
    public void SumarMonedas(int valorASumar)
    {
        if (canva != null)
        {
            totalMonedas += valorASumar;
            canva.ActualizarMonedas(valorASumar);
            //Debug.Log(totalMonedas);
        }
    }

    public void RestarMonedas(int valorARestar)
    {
        if (canva != null)
        {
            totalMonedas -= valorARestar;
            canva.ActualizarMonedas(valorARestar);
        }
    }

    public void SumarRubis(int valorASumar)
    {
        if (canva != null)
        {
            totalRubis += valorASumar;
            canva.ActualizarRubis(valorASumar);
        }
    }

    public void RestarRubis(int valorARestar)
    {
        if (canva != null)
        {
            totalRubis -= valorARestar;
            canva.ActualizarRubis(valorARestar);
        }
    }

    public void SumarPuntos(int puntosASumar)
    {
        if (canva != null)
        {
            puntosTotales += puntosASumar;
            canva.ActualizarPuntos(puntosASumar);
            Debug.Log(puntosTotales);
        }
    }
}
